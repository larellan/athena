#include "TRT_ConditionsAlgs/TRTCondWrite.h"
#include "TRT_ConditionsAlgs/TRTStrawAlign.h"
#include "TRT_ConditionsAlgs/TRTStrawStatusWrite.h"
#include "../TRTAlignCondAlg.h"


DECLARE_COMPONENT( TRTCondWrite )
DECLARE_COMPONENT( TRTStrawAlign )
DECLARE_COMPONENT( TRTStrawStatusWrite )
DECLARE_COMPONENT( TRTAlignCondAlg )
