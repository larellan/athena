#include "SCT_Monitoring/SCTMotherTrigMonTool.h"
#include "SCT_Monitoring/SCTTracksMonTool.h"
#include "SCT_Monitoring/SCTLorentzMonTool.h"
#include "SCT_Monitoring/SCTRatioNoiseMonTool.h"
#include "SCT_Monitoring/SCTErrMonTool.h"
#include "SCT_Monitoring/SCTHitEffMonTool.h"
#include "SCT_Monitoring/SCTHitsNoiseMonTool.h"
#include "SCT_Monitoring/SCTSummaryMonTool.h"

using namespace SCT_Monitoring;

DECLARE_COMPONENT( SCTMotherTrigMonTool )
DECLARE_COMPONENT( SCTTracksMonTool )
DECLARE_COMPONENT( SCTLorentzMonTool )
DECLARE_COMPONENT( SCTRatioNoiseMonTool )
DECLARE_COMPONENT( SCTErrMonTool )
DECLARE_COMPONENT( SCTHitEffMonTool )
DECLARE_COMPONENT( SCTHitsNoiseMonTool )
DECLARE_COMPONENT( SCTSummaryMonTool )

